#!/usr/bin/python
import subprocess, time, json, requests, datetime
from dateutil import parser
from pathlib import Path

separator =  '•'


def temperature():
    ''' CPU and GPU temp readings '''
    cpu_id = 'k10temp-pci-00c3'
    cpu_sensor_ids = [('Tctl','temp1_input'), ('Tccd1','temp3_input'), ('Tccd2','temp4_input')]
    gpu_id = 'amdgpu-pci-0900'
    gpu_sensor_ids = [('mem','temp3_input'), ('edge','temp1_input'), ('junction','temp2_input')]
    
    # write sensor data to file
    subprocess.run('sensors -j > /tmp/sensors', shell=True)
    with open('/tmp/sensors', 'r') as sensors_file:
        sensor_data = json.load(sensors_file)

    cpu_temp = 0
    for sid in cpu_sensor_ids:
        temp = sensor_data[cpu_id][sid[0]][sid[1]]
        if temp > cpu_temp:
            cpu_temp = temp
    gpu_temp = 0
    for sid in gpu_sensor_ids:
        temp = sensor_data[gpu_id][sid[0]][sid[1]]
        if temp > gpu_temp:
            gpu_temp = temp

    return int(cpu_temp), int(gpu_temp)

def tasks():
    r = requests.get('http://localhost:16661/list')
    tasks = r.json()
    count = 0
    wdeadline = 0
    for t in tasks:
        if t['Done']:
            continue
        if t['Deadline'] and t['Deadline'] != '0001-01-01T00:00:00Z':
            deadline = parser.parse(t['Deadline'])
            delta = deadline.date() - datetime.datetime.today().date()
            if delta.days <= 0:
                wdeadline = wdeadline + 1
        count = count + 1
    return count, wdeadline

def main():
    while True:
        cpu_temp, gpu_temp = temperature()
        ts, deadts = tasks()
        bartxt = f'Tasks:{ts} PastDue:{deadts} {separator} 🌡 CPU:{cpu_temp}° GPU:{gpu_temp}°'

        with open(str(Path.home()) + '/.config/custombar', 'w') as f:
            f.write(bartxt)

        time.sleep(5)

if __name__ == "__main__":
    main()
