# Custom Bar

A python script for displaying whatever you need to i3-status. The script writes to a file
`~/.config/custombar` which i3-status reads using this config segment:

```
read_file CUSTOM {
	color_good = "#ffffff"	
    format = "%content"
    path = "~/.config/custombar"
}
```

Custombar now writes CPU and GPU temperatures and uses the `sensors` command to do that but it can
be used to write whatever. 

Custombar is run through a systemd service. The provided service file needs to be edited to point to 
`custombar.py` (wherever you clone it). Then the service is enabled by copying the service file to 
systemd's user service folder and enabling it:

```sh
cp custombar.service ~/.config/systemd/user/
systemctl enable --user custombar.service
systemctl start --user custombar.service
```

Note: `~/.config/systemd/user/` could be something else depending on your distro. On windows you
could use [demoniker](`https://github.com/Muterra/py_daemoniker) or similar to run custombar.
